<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Old Cards</h2>
                <table border="1">
                    <tr bgcolor="#6a4d37">
                        <th>Theme</th>
                        <th>Valuable</th>
                    </tr>
                    <xsl:for-each select="OldCards/OldCard">
                        <tr>
                            <td><xsl:value-of select="Theme"/></td>
                            <td><xsl:value-of select="Valuable"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>