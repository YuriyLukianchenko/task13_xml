package com.lukianchenko.controller;

import com.lukianchenko.model.OldCard;
import com.lukianchenko.model.Parser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Controller {
    public void parse(Parser parser){
        try{
             parser.parse();
        }
        catch(ParserConfigurationException | IOException | SAXException e){
            e.printStackTrace();
        }
    }

}
