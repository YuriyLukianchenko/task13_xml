package com.lukianchenko;

import com.lukianchenko.controller.Controller;
import com.lukianchenko.model.DomParser;
import com.lukianchenko.model.Parser;

public class Application {
    public static void main(String[] args) {
        Controller controller = new Controller();
        Parser p = new DomParser();
        controller.parse(p);
        ((DomParser) p).listOfOldCard.stream().forEach(System.out::println);
    }
}
