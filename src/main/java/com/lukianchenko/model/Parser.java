package com.lukianchenko.model;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

@FunctionalInterface
public interface Parser {
    public void parse() throws ParserConfigurationException, IOException, SAXException;
}
