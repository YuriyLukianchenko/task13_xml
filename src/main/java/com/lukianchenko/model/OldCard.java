package com.lukianchenko.model;

public class OldCard {
    private String theme;
    public Info info = new Info();
    private String[] authors;
    private String valuable;
    private double price;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public String getValuable() {
        return valuable;
    }

    public void setValuable(String valuable) {
        this.valuable = valuable;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    @Override
    public String toString(){
        return "OldCard's elemnts: " + " Theme= " + getTheme() + ";"
                + info;
    }

    public class Info{
        private String type;
        private boolean twosides;
        private int[] format = new int[2];
        private String country;
        private int year;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public boolean isTwosides() {
            return twosides;
        }

        public void setTwosides(boolean twosides) {
            this.twosides = twosides;
        }

        public int[] getFormat() {
            return format;
        }

        public void setFormat(int[] format) {
            this.format = format;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        @Override
        public String toString(){
            return " Info: (" + "Type= " + getType() +";"
                    + " twoSides= " + isTwosides() + ";"
                    + " Format: (" + "height= " + format[0] + ";"
                    + " width= " + format[1] + ")"
                    + " country= " + getCountry() +";"
                    + " year= " + getYear() + ")";
        }
    }
}
