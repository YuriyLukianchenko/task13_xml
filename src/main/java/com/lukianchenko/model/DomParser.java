package com.lukianchenko.model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DomParser implements Parser{
    public List<OldCard> listOfOldCard = new ArrayList<>();
    OldCard currentOldCard;

    public void parse() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db =dbf.newDocumentBuilder();
        Document d = db.parse(new File("src/main/resources/xml/OldCardsXML.xml"));

        Element element = d.getDocumentElement();
        //System.out.println(element.getTagName());

        NodeList nodeList = element.getChildNodes();
        getElements(nodeList);

    }
    public void getElements(NodeList nodeList){
        for (int i = 0; i < nodeList.getLength(); i++){
            if (nodeList.item(i) instanceof Element) {
                //System.out.println(((Element) nodeList.item(i)).getTagName());
                String tagName = ((Element) nodeList.item(i)).getTagName().toString();
                if(tagName == "OldCard"){
                    currentOldCard = new OldCard();
                    listOfOldCard.add(currentOldCard);
                }
                elementParser(tagName, nodeList, i);
                if(nodeList.item(i).hasChildNodes()){
                    getElements(nodeList.item(i).getChildNodes());
                }
            }
        }
    }
    public void elementParser(String tagName, NodeList nodeList, int i){
        switch(tagName){
            case "Theme":
                currentOldCard.setTheme(nodeList.item(i).getTextContent());
            case "Type":
                currentOldCard.info.setType(nodeList.item(i).getTextContent());
            default:
        }
    }
}
